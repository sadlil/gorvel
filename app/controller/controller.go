package controller

import (
	"gopkg.in/macaron.v1"
	"./../core/response"
)

func Hello(ctx *macaron.Context) {
	response.Show(ctx, "index")
}
