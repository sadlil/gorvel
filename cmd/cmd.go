package cmd

import (
	"github.com/spf13/cobra"
	"fmt"
)

var RootCmd = &cobra.Command{
	Use:   "gorvel",
	Short: "web framework for go inspired by laravel",
	Long: ``,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("gorvel is running in your compoter")
	},
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of gorvel",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("0.0.1-alpha")
	},
}

func InitCmd() {
	RootCmd.AddCommand(versionCmd);
	RootCmd.Execute();
}
