package router

import (
	"./../app"
	"gopkg.in/macaron.v1"
)

func Get(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("GET", urlPattern, handler...)
}

func Post(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("POST", urlPattern, handler...)
}

func Put(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("PUT", urlPattern, handler...)
}

func Patch(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("PATCH", urlPattern, handler...)
}

func Delete(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("DELETE", urlPattern, handler...)
}

func Options(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("OPTIONS", urlPattern, handler...)
}

func Head(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("HEAD", urlPattern, handler...)
}

func Any(urlPattern string, handler ...macaron.Handler) {
	app.AddRouteContext("*", urlPattern, handler...)
}

func Group(urlPattern string, fn func(), handler ...macaron.Handler) {
	app.AddGroupContext(urlPattern, fn, handler...)
}
